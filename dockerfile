# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/cardnumber-inventory.jar /cardnumber-inventory.jar
# run application with this command line[
CMD ["java", "-jar", "/cardnumber-inventory.jar"]
